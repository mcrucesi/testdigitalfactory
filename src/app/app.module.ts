import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RentaComponent } from './renta/renta.component';
import { FormIngresoDatosComponent } from './form-ingreso-datos/form-ingreso-datos.component';
import { FormRentaComponent } from './form-renta/form-renta.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FormIngresoDatosComponent,
    RentaComponent,
    FormRentaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

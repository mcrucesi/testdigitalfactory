import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class TestService {
  // Base url
  service = {
    navigator: 'unknown',
    os: 'unknown'
  }
  baseurl = 'http://localhost:8001';

  constructor(private http: HttpClient) { }

  postToApi(rut, celular, correo, renta) {
    //return this.http.post<[]>(this.baseurl + '/api/test', JSON.stringify({ rut: rut, celular: celular, correo: correo, renta: renta }), this.httpOptions)
    console.log("SERVICE::: ", rut, celular, correo, renta, this.baseurl + '/api/test', JSON.stringify(navigator.userAgent))

    if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1) {
      this.service.navigator = 'Opera';
    }
    else if (navigator.userAgent.indexOf("Chrome") != -1) {
      this.service.navigator = 'Chrome';
    }
    else if (navigator.userAgent.indexOf("Safari") != -1) {
      this.service.navigator = 'Safari';
    }
    else if (navigator.userAgent.indexOf("Firefox") != -1) {
      this.service.navigator = 'Firefox';
    }
    else if ((navigator.userAgent.indexOf("MSIE") != -1)) //IF IE > 10
    {
      this.service.navigator = 'IE';
    }

    if (navigator.userAgent.indexOf("Windows NT 10.0") != -1) this.service.os = "Windows 10";
    if (navigator.userAgent.indexOf("Windows NT 6.2") != -1) this.service.os = "Windows 8";
    if (navigator.userAgent.indexOf("Windows NT 6.1") != -1) this.service.os = "Windows 7";
    if (navigator.userAgent.indexOf("Windows NT 6.0") != -1) this.service.os = "Windows Vista";
    if (navigator.userAgent.indexOf("Windows NT 5.1") != -1) this.service.os = "Windows XP";
    if (navigator.userAgent.indexOf("Windows NT 5.0") != -1) this.service.os = "Windows 2000";
    if (navigator.userAgent.indexOf("Mac") != -1) this.service.os = "Mac/iOS";
    if (navigator.userAgent.indexOf("X11") != -1) this.service.os = "UNIX";
    if (navigator.userAgent.indexOf("Linux") != -1) this.service.os = "Linux";
    /*return this.http.post<Any>(this.baseurl + '/api/test', { rut: rut, celular: celular, correo: correo, renta: renta }, this.httpOptions)
      .pipe(
        catchError(this.errorHandler));*/


    // Http Headers
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'x-user-browser': this.service.navigator,
        'x-user-os': this.service.os
      })
    }

    var result = ''
    
    this.http.post<any>(this.baseurl + '/api/test', { rut: rut, celular: celular, correo: correo, renta: renta },httpOptions).subscribe(data => {
      console.log("RESULT::: ", data)
      result = data.result
    })

    return result;
  }

  // Error
  errorHandler(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}

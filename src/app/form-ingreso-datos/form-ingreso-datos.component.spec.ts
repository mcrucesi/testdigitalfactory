import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormIngresoDatosComponent } from './form-ingreso-datos.component';

describe('FormIngresoDatosComponent', () => {
  let component: FormIngresoDatosComponent;
  let fixture: ComponentFixture<FormIngresoDatosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormIngresoDatosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormIngresoDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

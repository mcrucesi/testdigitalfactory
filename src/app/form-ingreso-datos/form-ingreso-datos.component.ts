import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Home } from '../home';
import { Router } from "@angular/router";

@Component({
  selector: 'app-form-ingreso-datos',
  templateUrl: './form-ingreso-datos.component.html',
  styleUrls: ['./form-ingreso-datos.component.css']
})
export class FormIngresoDatosComponent implements OnInit {
  home: Home = {
    rut: '',
    celular: null,
    correo: ''
  };

  profileForm = new FormGroup({
    rut: new FormControl(this.home.rut, [
      Validators.required,
      Validators.minLength(12)
    ]),
    celular: new FormControl(this.home.celular, [
      Validators.required,
      Validators.minLength(9),
      Validators.nullValidator
    ]),
    correo: new FormControl(this.home.correo, [
      Validators.required,
      Validators.minLength(4),
      Validators.email
    ])
  });

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.home.rut = this.profileForm.value.rut
    this.home.celular = this.profileForm.value.celular
    this.home.correo = this.profileForm.value.correo
    console.log("DATOS FORM::: ", this.home.rut, this.home.celular, this.home.correo);
    this.goToRenta();
  }

  goToRenta() {
    //this.router.navigate(['/renta', this.home.rut]);
    this.router.navigateByUrl('/renta', { state: { rut: this.home.rut, celular: this.home.celular, correo: this.home.correo } });
  }

}

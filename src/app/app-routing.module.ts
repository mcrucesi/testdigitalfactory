import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  //{ path: 'renta', component: RentaComponent },
  { path: 'renta', loadChildren: () => import('./ingreso-renta/ingreso-renta.module').then(m => m.IngresoRentaModule) }
  //{ path: 'renta', loadChildren: () => import('./form-ingreso-renta/form-ingreso-renta.module').then(m => m.FormIngresoRentaModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

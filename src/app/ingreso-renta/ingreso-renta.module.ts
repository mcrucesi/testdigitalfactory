import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { IngresoRentaRoutingModule } from './ingreso-renta-routing.module';
import { IngresoRentaComponent } from './ingreso-renta.component';


@NgModule({
  declarations: [IngresoRentaComponent],
  imports: [
    CommonModule,
    IngresoRentaRoutingModule,
    ReactiveFormsModule
  ]
})
export class IngresoRentaModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoRentaComponent } from './ingreso-renta.component';

describe('IngresoRentaComponent', () => {
  let component: IngresoRentaComponent;
  let fixture: ComponentFixture<IngresoRentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoRentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoRentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

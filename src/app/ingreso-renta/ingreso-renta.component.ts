import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Renta } from '../renta';
import { Router } from "@angular/router";
import { TestService } from '../test.service';

@Component({
  selector: 'app-ingreso-renta',
  templateUrl: './ingreso-renta.component.html',
  styleUrls: ['./ingreso-renta.component.css']
})
export class IngresoRentaComponent implements OnInit {
  renta: Renta = {
    renta: null
  };

  home = {
    rut: '',
    celular: null,
    correo: ''
  }

  profileForm = new FormGroup({
    renta: new FormControl(this.renta.renta, [
      Validators.required,
      Validators.minLength(6),
      Validators.nullValidator
    ])
  });

  constructor(private router: Router, private testService: TestService) { }

  ngOnInit() {
    if (history.state.rut) {
      console.log("HISTORY", history.state.rut)
      this.home.rut = history.state.rut;
      this.home.celular = history.state.celular;
      this.home.correo = history.state.correo;
    }
    else {
      this.gotoHome();
    }
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.renta.renta = this.profileForm.value.renta
    //console.log("DATOS FORM::: ", this.renta.renta);
    
    this.testService.postToApi(this.home.rut, this.home.celular, this.home.correo, this.renta.renta);

    //console.log("RESPONSE::: ", JSON.stringify(result))
  }

  gotoHome() {
    this.router.navigate(['/']);
    //this.router.navigateByUrl('/');
  }

}

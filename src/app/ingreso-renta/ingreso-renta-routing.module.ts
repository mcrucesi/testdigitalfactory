import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IngresoRentaComponent } from './ingreso-renta.component';

const routes: Routes = [{ path: '', component: IngresoRentaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IngresoRentaRoutingModule { }

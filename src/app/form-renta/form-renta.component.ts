import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Renta } from '../renta';

@Component({
  selector: 'app-form-renta',
  templateUrl: './form-renta.component.html',
  styleUrls: ['./form-renta.component.css']
})
export class FormRentaComponent implements OnInit {

  renta: Renta = {
    renta: null
  };

  profileForm = new FormGroup({
    renta: new FormControl(this.renta.renta, [
      Validators.required,
      Validators.minLength(6),
      Validators.nullValidator
    ])
  });

  constructor() { }

  ngOnInit() {
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.renta.renta = this.profileForm.value.renta
    console.log("DATOS FORM::: ", this.renta.renta);
  }

}
